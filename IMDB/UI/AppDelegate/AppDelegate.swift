//
//  AppDelegate.swift
//  IMDB
//
//  Created by Cassius Pacheco on 27/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        //Defines the current environment
        Environment.currentEnvironment = Environment.Staging

        println("\n\n~~~~~~~~~Running \(Environment.currentEnvironment.rawValue) environment~~~~~~~~~\n\n")

        return true
    }
}