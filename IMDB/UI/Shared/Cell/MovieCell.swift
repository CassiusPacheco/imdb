//
//  MovieCell.swift
//  IMDB
//
//  Created by Cassius Pacheco on 28/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class MovieCell: UITableViewCell {
    @IBOutlet var posterImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var plotLabel: UILabel!
    @IBOutlet var directorLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!

    //MARK: setup methods

    func configureCell(movie: Movie) {
        self.titleLabel.text = movie.title
        self.directorLabel.text = movie.director
        self.ratingLabel.text = movie.imdbRating
        self.plotLabel.text = movie.plotDescription
        self.yearLabel.text = movie.year

        if let URL = NSURL(string: movie.posterStringURL) {
            self.posterImageView.sd_setImageWithURL(URL, placeholderImage: nil, options: .RetryFailed, completed: { [weak self] (image, error, type, URL) -> Void in
                self?.posterImageView.setImageWithAnimation(image)
                })
        }
    }
}