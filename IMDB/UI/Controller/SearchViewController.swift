//
//  SearchViewController.swift
//  IMDB
//
//  Created by Cassius Pacheco on 27/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!

    private var currentType = Type.Unknown
    private var request: RequestManager?

    lazy private var movies: [Movie] = [Movie]()

    //MARK: life cycle methods

    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchBar.becomeFirstResponder()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if let indexPath = self.tableView.indexPathForSelectedRow() {
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }

    //MARK: request methods

    private func search(title: String) {
        self.searchBar.userInteractionEnabled = false

        //keeping the 'request' allows us cancel/retry the request
        self.request = MovieService.search(title, type: self.currentType) { [weak self] (movies, error) -> () in
            //TODO: show a possible error to user
            self?.movies = movies
            self?.tableView.reloadData()
            self?.searchBar.userInteractionEnabled = true
            self?.request = nil
        }
    }

    //MARK: Action methods

    @IBAction func segmentedControlValueChanged(segmentedControl: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 1:
            self.currentType = .Movie
        case 2:
            self.currentType = .Series
        case 3:
            self.currentType = .Episode
        default:
            self.currentType = .Unknown
        }

        self.request?.cancelRequest()
        self.movies.removeAll(keepCapacity: false)
        self.tableView.reloadData()
    }

    //MARK: UITableViewDataSource methods

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! MovieCell
        cell.configureCell(self.movies[indexPath.row])

        return cell
    }

    //MARK: UITableViewDelegate methods

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row >= 0 && indexPath.row < self.movies.count {
            //This action could be a Segue and instead of instantiate this VC via code. 
            //We could've only set the selected Movie in 'prepareForSegue' method. 
            //I chose this approach to exemplify this kind of instantiation
            let movieDetailVC = MovieDetailViewController.instantiate(movie: self.movies[indexPath.row])
            self.navigationController?.pushViewController(movieDetailVC, animated: true)
        }
    }

    //MARK: UISearchBarDelegate methods

    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.search(searchBar.text)
    }
}