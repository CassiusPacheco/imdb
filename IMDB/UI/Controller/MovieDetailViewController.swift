//
//  MovieDetailViewController.swift
//  IMDB
//
//  Created by Cassius Pacheco on 28/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import UIKit
import TPKeyboardAvoiding

class MovieDetailViewController: UIViewController {
    private class var storyboardIdentifier: String { return "MovieDetailViewController" }

    @IBOutlet var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var actorsLabel: UILabel!
    @IBOutlet weak var plotLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!

    private var movie: Movie!

    //MARK: instantiate method

    class func instantiate(#movie: Movie) -> MovieDetailViewController {
        let movieDetailVC = UIStoryboard.mainStoryboard.instantiateViewControllerWithIdentifier(self.storyboardIdentifier) as! MovieDetailViewController
        movieDetailVC.movie = movie
        movieDetailVC.title = movie.title.uppercaseString

        return movieDetailVC
    }

    //MARK: life cycle methods

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupData()
        self.scrollView.contentSizeToFit()
    }

    //MARK: Data method

    private func setupData() {
        self.titleLabel.text = self.movie.title
        self.directorLabel.text = self.movie.director
        self.ratingLabel.text = self.movie.imdbRating
        self.plotLabel.text = self.movie.plotDescription
        self.yearLabel.text = self.movie.year
        self.writerLabel.text = self.movie.writer
        self.languageLabel.text = self.movie.language
        self.runtimeLabel.text = self.movie.runtime
        self.actorsLabel.text = self.movie.actors
        self.genreLabel.text = self.movie.genre

        if let URL = NSURL(string: self.movie.posterStringURL) {
            self.posterImageView.sd_setImageWithURL(URL, placeholderImage: nil, options: .RetryFailed, completed: { [weak self] (image, error, type, URL) -> Void in
                self?.posterImageView.setImageWithAnimation(image)
                })
        }
    }
}