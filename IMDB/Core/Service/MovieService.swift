//
//  MovieService.swift
//  IMDB
//
//  Created by Cassius Pacheco on 27/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import Alamofire

class MovieService: BaseService {
    class func search(title: String, year: String? = nil, type: Type, callback: (movies: [Movie], error: NSError?) -> ()) -> RequestManager {
        let parameters: [String: AnyObject] = ["t": title, "y": year ?? "", "type": type.rawValue, "plot": "full", "r": "json"]

        return self.newRequest("MovieService.search", URL: Router.Search(parameters)) { (request, response, json, error) in
            callback(movies: Movie.parseMovies(json), error: error)
        }
    }
}