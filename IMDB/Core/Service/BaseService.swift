//
//  BaseService.swift
//  IMDB
//
//  Created by Cassius Pacheco on 27/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class BaseService {
    class func newRequest(description: String, URL: URLRequestConvertible, callback: (NSURLRequest, NSHTTPURLResponse?, JSON, NSError?) -> Void) -> RequestManager {
        return RequestManager(description: description, URL: URL, callback: callback)
    }
}