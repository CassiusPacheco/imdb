//
//  RequestManager.swift
//  IMDB
//
//  Created by Cassius Pacheco on 27/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import Alamofire
import Async
import SwiftyJSON

class RequestManager {
    class var NoInternetConnectionNotification: String { return "NoInternetConnection" }
    class var ServerErrorNotification: String { return "ServerError" }

    private var requestObject: Request?
    private let description: String
    private let URL: URLRequestConvertible
    private let callback: (NSURLRequest, NSHTTPURLResponse?, JSON, NSError?) -> Void

    init(description: String, URL: URLRequestConvertible, callback: (NSURLRequest, NSHTTPURLResponse?, JSON, NSError?) -> Void) {
        self.description = description
        self.URL = URL
        self.callback = callback

        self.retryRequest()
    }

    func cancelRequest() -> Void {
        self.requestObject?.cancel()
    }

    func retryRequest() -> Void {
        if RequestManager.hasInternet() {
            self.requestObject = self.createRequest()
        } else {
            self.notifyNoInternetConnection()
        }
    }

    private func createRequest() -> Request {
        RequestManager.incrementNetworkActivityCount()

        return request(self.URL).responseJSON(options: NSJSONReadingOptions.AllowFragments, completionHandler: { (request, response, json, error) -> Void in
            RequestManager.decrementNetworkActivityCount()

            Async.userInteractive() {
                println("---\(self.description):\n\(json)\n\(response)\n\(error)")

                Async.main() {
                    if response?.statusCode == 500 || response?.statusCode == 404 {
                        self.notifyServerError()
                    } else if response?.statusCode == 401 {
                        //unauthenticated: this error should be treated, probably asking for login again
                    } else {
                        self.callback(request, response, JSON(json ?? NSNull()), error)
                    }
                }
            }
        })
    }

    private func notifyNoInternetConnection() -> Void {
        //Send a notification to show an alert to user and/or update some interfaces
        NSNotificationCenter.defaultCenter().postNotificationName(RequestManager.NoInternetConnectionNotification, object: self)
    }

    private func notifyServerError() -> Void {
        //Send a notification to show an alert to user and/or update some interfaces
        NSNotificationCenter.defaultCenter().postNotificationName(RequestManager.ServerErrorNotification, object: self)
    }

    class func decrementNetworkActivityCount() {
        AFNetworkActivityIndicatorManager.sharedManager().decrementActivityCount()
    }

    class func incrementNetworkActivityCount() {
        AFNetworkActivityIndicatorManager.sharedManager().incrementActivityCount()
    }

    class func hasInternet() -> Bool {
        let reachability: Reachability = Reachability.reachabilityForInternetConnection()
        return reachability.currentReachabilityStatus().value != NotReachable.value
    }
}