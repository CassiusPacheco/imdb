//
//  UIImage+Additions.swift
//  IMDB
//
//  Created by Cassius Pacheco on 28/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {

    func setImageWithAnimation(image: UIImage?, duration: NSTimeInterval = 0.35) {
        UIView.transitionWithView(self, duration: duration, options: .TransitionCrossDissolve, animations: { () -> Void in
            self.image = image
            }, completion: nil)
    }
}