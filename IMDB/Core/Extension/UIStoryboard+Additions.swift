//
//  UIStoryboard+Additions.swift
//  IMDB
//
//  Created by Cassius Pacheco on 28/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    //We must define all project's storyboards here
    //e.g. class var loginStoryboard: UIStoryboard { return UIStoryboard(name: "Login", bundle: nil) }
    class var mainStoryboard: UIStoryboard { return UIStoryboard(name: "Main", bundle: nil) }
}