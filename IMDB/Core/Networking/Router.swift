//
//  Router.swift
//  IMDB
//
//  Created by Cassius Pacheco on 27/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    case Search([String: AnyObject])

    var method: Alamofire.Method {
        switch self {
            //This example is too simple, we only use .GET, but we can define methods for specific routes types
            //e.g. case .CreateUser, .Login: return .POST
        default:
            return .GET
        }
    }

    var path: String {
        switch self {
            //In this example we're not using custom paths, but it's useful fo define endpoints for specific router types
            //e.g. case .CreateUser: return "/users/create"
        case .Search:
            return ""
        }
    }

    var parameters: [String: AnyObject]? {
        switch self {
        case .Search(let parameters):
            return parameters
        default:
            return nil
        }
    }

    var version: String {
        switch self {
            //we can define version for specific routes types and concatenate it in the URLRequest
            //e.g case .Messages, .ProductList: return "2"
        default:
            return ""
        }
    }

    // MARK: URLRequestConvertible

    var URLRequest: NSURLRequest {
        let URLRequest = NSURLRequest(URL: Environment.baseAPIURL.URLByAppendingPathComponent(self.path))
        //Here we can setup Authentication token, define specific URL encode for which type of route, etc.

        return Alamofire.ParameterEncoding.URL.encode(URLRequest, parameters: self.parameters).0
    }
}