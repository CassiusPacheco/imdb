//
//  Environment.swift
//  IMDB
//
//  Created by Cassius Pacheco on 27/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation

enum Environment: String {
    case Localhost = "localhost"
    case Staging = "staging"
    case Production = "production"

    //MARK: Public static methods

    static var currentEnvironment: Environment = .Staging {
        didSet {
            self.environmentsDictionary = nil
        }
    }

    static var baseURL: NSURL! {
        return NSURL(string: self.environmentValueForKey("base_url") as! String)
    }

    static var baseAPIURL: NSURL! {
        return NSURL(string: self.environmentValueForKey("base_api_url") as! String)
    }

    static var googleAnalyticsTrackingId: String {
        return self.environmentValueForKey("google_analytics_tracking_id") as! String
    }

    static var isLocalhost: Bool {
        return self.currentEnvironment == .Localhost
    }

    static var isStaging: Bool {
        return self.currentEnvironment == .Staging
    }

    static var isProduction: Bool {
        return self.currentEnvironment == .Production
    }

    //MARK: Private static methods

    private static var environmentsDictionary: [String: AnyObject]?

    private static func environmentValueForKey(key: String) -> AnyObject {
        if self.environmentsDictionary == nil {
            let root = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("Environments", ofType: "plist")!)!
            self.environmentsDictionary = root[self.currentEnvironment.rawValue] as? [String: AnyObject]
        }

        return self.environmentsDictionary![key]!
    }
}