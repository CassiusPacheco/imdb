//
//  Type.swift
//  IMDB
//
//  Created by Cassius Pacheco on 27/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation

enum Type: String {
    case Movie = "movie"
    case Series = "series"
    case Episode = "episode"
    case Unknown = ""
}