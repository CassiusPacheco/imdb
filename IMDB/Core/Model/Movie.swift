//
//  Movie.swift
//  IMDB
//
//  Created by Cassius Pacheco on 27/08/2015.
//  Copyright (c) 2015 Cassius Pacheco. All rights reserved.
//

import Foundation
import SwiftyJSON

class Movie {
    var imdbId = ""
    var title = ""
    var year = ""
    var rated = ""
    var released = ""
    var runtime = ""
    var genre = ""
    var director = ""
    var writer = ""
    var actors = ""
    var plotDescription = ""
    var language = ""
    var awards = ""
    var posterStringURL = ""
    var imdbRating = ""
    var type = Type.Unknown

    //MARK: init method

    init() { }

    //MARK: Parse methods

    class func parseMovies(json: JSON) -> [Movie] {
        var movies = [Movie]()

        switch json.type {
        case SwiftyJSON.Type.Array where json.array != nil:
            for j in json.arrayValue {
                movies.append(Movie.parse(j))
            }
        default:
            movies.append(Movie.parse(json))
        }

        return movies
    }

    class func parse(json: JSON) -> Movie {
        var movie = Movie()
        movie.imdbId = json["ImdbID"].stringValue
        movie.title = json["Title"].stringValue
        movie.year = json["Year"].stringValue
        movie.rated = json["Rated"].stringValue
        movie.released = json["Released"].stringValue
        movie.runtime = json["Runtime"].stringValue
        movie.genre = json["Genre"].stringValue
        movie.director = json["Director"].stringValue
        movie.writer = json["Writer"].stringValue
        movie.actors = json["Actors"].stringValue
        movie.plotDescription = json["Plot"].stringValue
        movie.language = json["Language"].stringValue
        movie.awards = json["Awards"].stringValue
        movie.posterStringURL = json["Poster"].stringValue
        movie.imdbRating = json["imdbRating"].stringValue

        if let type = json["Type"].string {
            movie.type = Type(rawValue: type) ?? .Unknown
        }
        
        return movie
    }
}